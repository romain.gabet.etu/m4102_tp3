

| URI                      | Opération    |  MIME                                                         | Requête         | Réponse                                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |
| /pizzas             | GET         | <-application/json<br><-application/xml                      |                 | liste des pizzas (P2)                                           |
| /pizzas/{id}        | GET         | <-application/json<br><-application/xml                      |                 | une pizza (P2) ou une erreur 404                                            |
| /pizzas/{id}/name   | GET         | <-text/plain                                                 |                 | le nom de la pizza ou 404                                        |
| /pizzas             | POST        | <-/->application/json<br>-> | Pizza (P1) | Nouvelle pizza (P2)<br>409 si la pizza existe déjà (même nom) ou si a moins un des ingredients de la pizza n'existe pas |
| /pizzas/{id}        | DELETE      |                                                              |                 |                                                                      |




Une pizza comporte uniquement un identifiant, un nom et une liste d'ingédients. Sa représentation JSON (P2) prendra donc la forme suivante :
```
{
  "id": "52f68024-24ec-46c0-8e77-c499dba1e27e",
  "name": "Reine",
  "ingredients": ["52f68024-24ec-46c0-8e77-c499dba1e27e"]
}
```

Lors de la création, l'identifiant n'est pas connu car il sera fourni par le JavaBean qui représente un ingrédient. Aussi on aura une représentation JSON (P1) qui comporte uniquement le nom et la liste des ingrédients :
```
{
  "name": "Reine",
  "ingredients": ["52f68024-24ec-46c0-8e77-c499dba1e27e"]
}
```
