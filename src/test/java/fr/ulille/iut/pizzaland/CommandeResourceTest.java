package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Response;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;


/**
 * @author gabetr
 * Class de test des intéractions possible avec Commande
 */
public class CommandeResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(CommandeResourceTest.class.getName());

	private static String userName = "myName";

	private PizzaDao dao;
	private IngredientDao daoIngredient;
	private CommandeDao daoCommandes;

	//Configure section

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();

		return new ApiV1();

	}

	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(PizzaDao.class);
		dao.createTableAndIngredientAssociation();

		daoIngredient = BDDFactory.buildDao(IngredientDao.class);
		daoIngredient.createTable();  

		IngredientCreateDto test1 = new IngredientCreateDto();
		test1.setName("test1");
		IngredientCreateDto test2 = new IngredientCreateDto();
		test2.setName("test2");
		target("/ingredients").request().post(Entity.json(test1));
		target("/ingredients").request().post(Entity.json(test2));

		ArrayList<Ingredient> ingredients = new ArrayList<>();
		ingredients.add(daoIngredient.findByName("test1"));
		ingredients.add(daoIngredient.findByName("test2"));

		Pizza pizza = new Pizza();
		pizza.setName("Reine");
		pizza.setIngredients(ingredients);
		dao.insertPizzaWithIngredients(pizza);

		daoCommandes = BDDFactory.buildDao(CommandeDao.class);
		daoCommandes.createTable();
	}

	@After
	public void tearEnvDown() throws Exception {
		daoCommandes.dropTable();
		daoIngredient.dropTable();
		dao.dropTable();

	}

	//Getter and setter section

	
	@Test
	public void testGetUnexistingPizzas() {
		PizzaDto pizza = new PizzaDto();
		pizza.setName("Reine");

		Response response = target("/commandes").path(userName).request().post(Entity.json(pizza));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testGetExistingCommand() {
		Pizza pizza = dao.findByName("Reine");
		daoCommandes.insertPizza("myName", pizza);

		Response response = target("/commandes/" + userName).request(MediaType.APPLICATION_JSON).get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		List<PizzaDto> result = response.readEntity(new GenericType<List<PizzaDto>>() {});
		assertEquals(pizza, Pizza.fromDto(result.get(0)));
	}

	@Test
	public void testGetEmptyList() {
		Response response = target("/commandes/" + userName).request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {});

		assertEquals(0, pizzas.size());
	}

	
	@Test
	public void testSetPizzaIntoCommande() {
		Pizza pizza = dao.findByName("Reine");

		Response response = target("/commandes/" + userName).request().post(Entity.json(Pizza.toDto(pizza)));
		
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		assertEquals(pizza, daoCommandes.getAll(userName).get(0));
	}
	
//Remove section

	@Test
	public void testRemoveExistingPizza() {
		Pizza pizza = dao.findByName("Reine");
		daoCommandes.insertPizza(userName, pizza);

		Response response = target("/commandes/" + userName).path(pizza.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		List<Pizza> result = daoCommandes.getAll(userName);
		assertEquals(0, result.size());
	}

	@Test
	public void testRemoveNotExistingPizza() {
		Response response = target("/commandes/" + userName).path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

}
