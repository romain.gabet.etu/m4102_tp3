package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * @author gabetr
 * Class de test des intéractions possible avec Pizza
 */
public class PizzaResourceTest extends JerseyTest {
	private PizzaDao dao;
	private IngredientDao daoIngredient;

	//Configure section

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		return new ApiV1();
	}

	@Before
	public void setEnvUp() {

		dao = BDDFactory.buildDao(PizzaDao.class);
		dao.createTableAndIngredientAssociation();

		daoIngredient = BDDFactory.buildDao(IngredientDao.class);
		daoIngredient.createTable();

		IngredientCreateDto testDto1 = new IngredientCreateDto();
		testDto1.setName("test1");
		target("/ingredients").request().post(Entity.json(testDto1));

		IngredientCreateDto testDto2 = new IngredientCreateDto();
		testDto2.setName("test2");
		target("/ingredients").request().post(Entity.json(testDto2));
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTable();
	}

	//Create section


	/*	@Test
	public void testCreateWithForm() {
		Form form = new Form();
		form.param("name", "reine");

		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("pizzas").request().post(formEntity);

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Pizza result = dao.findById(UUID.fromString(id));

		assertNotNull(result);
	}
	 */

	@Test
	public void testCreatePizzaWithIngredient() {
		ArrayList<Ingredient> ingredients = new ArrayList<>();
		ingredients.add(daoIngredient.findByName("test1"));
		ingredients.add(daoIngredient.findByName("test2"));

		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Reine");
		pizzaCreateDto.setIngredients(ingredients);
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);
		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
	}

	@Test
	public void testCreatePizzaWithoutIngredients() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("test");

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreatePizzaWithMissingIngredients() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("test");
		List<Ingredient> ingredients = new ArrayList<>();
		ingredients.add(new Ingredient("Missing"));
		pizzaCreateDto.setIngredients(null);

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}   

	@Test
	public void testCreateSamePizza() {
		IngredientCreateDto test1 = new IngredientCreateDto();
		IngredientCreateDto test2 = new IngredientCreateDto();
		test2.setName("test2");
		test1.setName("test1");

		target("/ingredients").request().post(Entity.json(test1));
		target("/ingredients").request().post(Entity.json(test2));

		ArrayList<Ingredient> ingredients = new ArrayList<>();
		ingredients.add(daoIngredient.findByName("test1"));
		ingredients.add(daoIngredient.findByName("test2"));

		Pizza pizza = new Pizza();
		pizza.setName("Reine");
		pizza.setIngredients(ingredients);

		dao.insertPizzaWithIngredients(pizza);
		PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreatePizzaWithoutName() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));
		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}

	//Getter and setter section
	@Test
	public void testGetExistingPizza() {
		ArrayList<Ingredient> ingredients = new ArrayList<>();
		ingredients.add(daoIngredient.findByName("test1"));
		ingredients.add(daoIngredient.findByName("test2"));

		Pizza pizza = new Pizza();
		pizza.setName("Reine");
		pizza.setIngredients(ingredients);
		dao.insertPizzaWithIngredients(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());


		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
	}

	@Test
	public void testGetNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}


	@Test
	public void testGetPizzaName() {
		ArrayList<Ingredient> ingredients = new ArrayList<>();
		ingredients.add(daoIngredient.findByName("test1"));
		ingredients.add(daoIngredient.findByName("test2"));

		Pizza pizza = new Pizza();
		pizza.setName("Reine");
		pizza.setIngredients(ingredients);
		dao.insertPizzaWithIngredients(pizza);

		Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();
		
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals("Reine", response.readEntity(String.class));
	}

	@Test
	public void testGetNotExistingPizzaName() {
		Response response = target("pizzas").path(UUID.randomUUID().toString()).path("name").request().get();
		
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());

	}

	@Test
	public void testGetEmptyList() { 
		Response response = target("/pizzas").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {});

		assertEquals(0, pizzas.size());
	}
	@Test
	public void testGetList() {

		ArrayList<Ingredient> ingredients = new ArrayList<>();
		ingredients.add(daoIngredient.findByName("test1"));
		ingredients.add(daoIngredient.findByName("test2"));

		Pizza pizza = new Pizza();
		pizza.setName("Reine");
		pizza.setIngredients(ingredients);
		dao.insertPizzaWithIngredients(pizza);
		Response response = target("/pizzas").request().get();
	
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {});

		assertEquals(pizza, Pizza.fromDto(pizzas.get(0)));
	}
	//Delete section

	@Test
	public void testDeleteExistingPizza() {
		ArrayList<Ingredient> ingredients = new ArrayList<>();
		ingredients.add(daoIngredient.findByName("test1"));
		ingredients.add(daoIngredient.findByName("test2"));

		Pizza pizza = new Pizza();
		pizza.setName("Reine");
		pizza.setIngredients(ingredients);
		dao.insertPizzaWithIngredients(pizza);
		Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();
		
		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());
		Pizza result = dao.findById(pizza.getId());

		assertEquals(result, null);
	}

	@Test
	public void testDeleteNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}



}
