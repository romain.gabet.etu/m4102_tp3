package fr.ulille.iut.pizzaland.beans;

import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;

/**
 * @author gabetr
 * Classe décrivant le composant Ingredient et les intéractions possibles
 */
public class Ingredient {
    private UUID id = UUID.randomUUID();
    private String name;
    
 //Constructor section

    public Ingredient() {
    }

    /**
     * @param name
     */
    public Ingredient(String name) {
        this.name = name;
    }

    /**
     * @param id
     * @param name
     */
    public Ingredient(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

//Getter and setter section
    /**
     * @param id
     */
    public void setId(UUID id) {
        this.id = id;
    }

    /**
     * @return
     */
    public UUID getId() {
        return id;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

//Dto section
    
    /**
     * @param i
     * @return
     */
    public static IngredientDto toDto(Ingredient i) {
        IngredientDto dto = new IngredientDto();
        dto.setId(i.getId());
        System.out.println("test --------------------\n\n\n"+i.getName());
        dto.setName(i.getName());

        return dto;
    }
    
    /**
     * @param dto
     * @return
     */
    public static Ingredient fromDto(IngredientDto dto) {
        Ingredient ingredient = new Ingredient();
        ingredient.setId(dto.getId());
        ingredient.setName(dto.getName());

        return ingredient;
    }
    
//Dto create section
    
    /**
     * @param ingredient
     * @return
     */
    public static IngredientCreateDto toCreateDto(Ingredient ingredient) {
        IngredientCreateDto dto = new IngredientCreateDto();
        dto.setName(ingredient.getName());
            
        return dto;
      }
    	
      /**
     * @param dto
     * @return
     */
    public static Ingredient fromIngredientCreateDto(IngredientCreateDto dto) {
        Ingredient ingredient = new Ingredient();
        ingredient.setName(dto.getName());

        return ingredient;
      }
  
//Equals section
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ingredient other = (Ingredient) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

//To string section	

	@Override
    public String toString() {
        return "Ingredient [id=" + id + ", name=" + name + "]";
    }
}
