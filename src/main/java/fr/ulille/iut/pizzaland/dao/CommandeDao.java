package fr.ulille.iut.pizzaland.dao;


import java.util.List;
import java.util.UUID;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import fr.ulille.iut.pizzaland.beans.Pizza;

/**
 * @author gabetr
 * Class décrivant les intéraction entre l'élément Commande et la base de donnée 
 */

public interface CommandeDao {

  @SqlUpdate("CREATE TABLE IF NOT EXISTS commandes (name VARCHAR(128) PRIMARY KEY, idPizza int, FOREIGN KEY(idPizza) REFERENCES pizzas(id))")
  void createTable();

  @SqlQuery("SELECT p.* FROM commandes AS c INNER JOIN pizzas AS p ON c.idPizza = p.id WHERE c.name = :name")
  @RegisterBeanMapper(Pizza.class)
  List<Pizza> getAll(@Bind("name") String name);

  @SqlUpdate("INSERT INTO commandes (name, idPizza) VALUES (:cname, :pizza.id)")
  boolean insertPizza(@Bind("cname") String cname, @BindBean("pizza") Pizza pizza);

  @SqlUpdate("DELETE FROM commandes WHERE name = :name and idPizza = :id")
  @RegisterBeanMapper(Pizza.class)
  boolean remove(@Bind("name") String name, @Bind("id") UUID id);

  @SqlUpdate("DROP TABLE commandes")
  void dropTable();

}
