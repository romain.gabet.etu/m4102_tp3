package fr.ulille.iut.pizzaland.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.BindList;
import org.jdbi.v3.sqlobject.statement.SqlBatch;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

/**
 * @author gabetr
 * Class décrivant les intéraction entre l'élément Pizza et la base de donnée (pizzas et pizzasIngredientAssociation)
 */
public interface PizzaDao {

	//Create section

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzaIngredientsAssociation (idPizza int NOT NULL, idIngredient int NOT NULL, "
			+ "PRIMARY KEY(idPizza, idIngredient), "
			+ "FOREIGN KEY(idIngredient) REFERENCES ingredients(id), "
			+ "FOREIGN KEY(idPizza) REFERENCES pizzas(id))")
	void createAssociationTable();

	@Transaction
	default void createTableAndIngredientAssociation() {
		createAssociationTable();
		createPizzaTable();
	}

	//Getter and setter section

	@Transaction
	default List<Pizza> getAll() {
		ArrayList<Pizza> pizzas = new ArrayList<>();
		for (Pizza pizza : getAllPizzas()) {
			pizza.setIngredients(getIngredientsForPizza(pizza.getId()));
			pizzas.add(pizza);
		}
		return pizzas;
	}

	@SqlQuery("SELECT * FROM pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAllPizzas();

	@SqlQuery("SELECT * FROM pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findPizzaById(@Bind("id") UUID id);

	@SqlQuery("SELECT * FROM pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(@Bind("name") String name);

	@SqlQuery("SELECT * FROM ingredients as i INNER JOIN pizzaIngredientsAssociation AS pia ON pia.idIngredient = i.id WHERE pia.idPizza = :id")
	@RegisterBeanMapper(Ingredient.class)
	List<Ingredient> getIngredientsForPizza(@Bind("id") UUID id);

	@Transaction
	default Pizza findById(UUID id){
		Pizza pizza = findPizzaById(id);
		if (pizza == null)
			return null;
		pizza.setIngredients(getIngredientsForPizza(id));
		return pizza;
	}

	//Insert section

// Version 1	
	@SqlBatch("INSERT INTO pizzaIngredientsAssociation (idPizza, idIngredient) VALUES (:idPizza, :ingredient.id)")
	void insertIngredients(@Bind("idPizza") UUID idPizza, @BindBean("ingredient") List<Ingredient> ingredients);
	
/* Version 2
	@SqlUpdate("INSERT INTO pizzaIngredientsAssociation (idPizza, idIngredient) VALUES (:idPizza, :idIngredient)")
	void insertIngredient(@Bind("idPizza") UUID idPizza, @Bind("idIngredient") UUID idIngredient);

	@Transaction
	default void insertIngredients(UUID idPizza, List<Ingredient> ingredients) {
		
		for(int i=0; i<ingredients.size();i++) {
			insertIngredient(idPizza, ingredients.get(i).getId());
		}
	}
*/
	@SqlUpdate("INSERT INTO pizzas (id, name) VALUES (:id, :name)")
	void insertPizza(@BindBean Pizza pizza);

	@Transaction
	default void insertPizzaWithIngredients(Pizza pizza) {
		insertPizza(pizza);
		insertIngredients(pizza.getId(), pizza.getIngredients());
	}


	//Remove section

	@SqlUpdate("DELETE FROM pizzas WHERE id = :id")
	void removeId(@Bind("id") UUID id);

	@SqlUpdate("DELETE FROM pizzaIngredientsAssociation WHERE idPizza = :id")
	void removeAssociationId(@Bind("id") UUID id);

	@Transaction
	default void remove(UUID id) {
		removeId(id);
		removeAssociationId(id);
	}

	//Drop section

	@SqlUpdate("DROP TABLE pizzas")
	void dropTablePizza();
	@SqlUpdate("DROP TABLE pizzaIngredientsAssociation")
	void dropAssocation();

	@Transaction
	default void dropTable() {
		dropTablePizza();
		dropAssocation();
	}


}
