package fr.ulille.iut.pizzaland.resources;

import java.sql.SQLException;

import fr.ulille.iut.pizzaland.BDDFactory;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;


/**
 * @author gabetr
 *	Class permettant de nétoyer les tables de la base de donnée
 */
@Path("clearDatabase")
public class BDDClearResource {

    @GET
    public void clearDatabase()  throws SQLException {
        BDDFactory.dropTables();
    }
}
