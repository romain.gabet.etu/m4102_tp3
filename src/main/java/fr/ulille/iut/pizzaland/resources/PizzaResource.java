package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

/**
 * @author gabetr
 * class décrivant les diférentes intéraction avec Pizza avec l'envoi d'une requette http
 */
@Produces("application/json")
@Path("/pizzas")
public class PizzaResource {
	private static final Logger LOGGER = Logger.getLogger(PizzaResource.class.getName());

	private PizzaDao pizzas;

	@Context
	public UriInfo uriInfo;

	//Constructor section

	public PizzaResource() {
		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createPizzaTable();

	}

	//Create section

	/**
	 * @param pizzaCreateDto
	 * @return
	 */
	@POST
	public Response createPizza(PizzaCreateDto pizzaCreateDto) {
		Pizza existing = pizzas.findByName(pizzaCreateDto.getName());
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Pizza pizza = Pizza.fromPizzaCreateDto(pizzaCreateDto);
			pizzas.insertPizzaWithIngredients(pizza);
			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}

	}

	//Get section	

	/**
	 * @return
	 */
	@GET
	public List<PizzaDto> getAll() {
		LOGGER.info("PizzaResource:getAll");

		List<PizzaDto> PizzaList = pizzas.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
		LOGGER.info(PizzaList.toString());
		return PizzaList;
	}

	/**
	 * @param id
	 * @return
	 */
	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml" })
	public PizzaDto getOnePizza(@PathParam("id") UUID id) {
		LOGGER.info("getOnePizza(" + id + ")");
		try {
			Pizza pizza = pizzas.findById(id);
			LOGGER.info(pizza.toString());
			return Pizza.toDto(pizza);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}


	/**
	 * @param id
	 * @return
	 */
	@GET
	@Path("{id}/name")
	public String getPizzaName(@PathParam("id") UUID id) {
		try {
			Pizza pizza = pizzas.findById(id);
			pizza.setIngredients(pizzas.getIngredientsForPizza(id));

			return pizza.getName();
		}
		catch (NullPointerException e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);    		
		}

	}

	//Delete section

	/**
	 * @param id
	 * @return
	 */
	@DELETE
	@Path("{id}")
	public Response deletePizza(@PathParam("id") UUID id) {
		if ( pizzas.findById(id) == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		pizzas.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}




}

