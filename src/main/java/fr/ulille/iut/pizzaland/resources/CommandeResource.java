package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.WebApplicationException;
import fr.ulille.iut.pizzaland.BDDFactory;
import jakarta.ws.rs.core.UriInfo;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;

/**
 * @author gabetr
 *	Class permettant de nétoyer les tables de la base de donnée
 */
@Produces("application/json")
@Path("/commandes")
public class CommandeResource {
	private static final Logger LOGGER = Logger.getLogger(CommandeResource.class.getName());
	private CommandeDao cmds;


	@Context
	public UriInfo uriInfo;

	public CommandeResource() {
		cmds = BDDFactory.buildDao(CommandeDao.class);
		cmds.createTable();
	}

	/**
	 * @param name
	 * @return
	 */
	@GET
	@Path("{name}")
	public List<PizzaDto> getAll(@PathParam("name") String name) {
		List<PizzaDto> pizzaList = cmds.getAll(name).stream().map(Pizza::toDto).collect(Collectors.toList());
		return pizzaList;
	}

	/**
	 * @param name
	 * @param pizzaDto
	 * @return
	 */
	@POST
	@Path("{name}")
	public Response addPizza(@PathParam("name") String name, PizzaDto pizzaDto) {
		try {
			Pizza pizza = Pizza.fromDto(pizzaDto);
			cmds.insertPizza(name, pizza);
			URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();
			return  Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}

	/**
	 * @param name
	 * @param id
	 * @return
	 */
	@DELETE
	@Path("{name}/{id}")
	public Response deletePizza(@PathParam("name") String name, @PathParam("id") UUID id) {
		try {
			if (!cmds.remove(name, id)) {
				throw new WebApplicationException(Response.Status.NOT_FOUND);
			}
			return Response.status(Response.Status.ACCEPTED).build();
		} catch(Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

}

