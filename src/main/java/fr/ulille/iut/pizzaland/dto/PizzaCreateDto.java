package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

/**
 * @author gabetr
 * Classe décrivant la création de l'élément Pizza dans le dto avec la liste des Ingredient
 */
public class PizzaCreateDto {


	private String name;
	private List<Ingredient> ingredients;
	
	public PizzaCreateDto() {}
		
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
 		
	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return
	 */
	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	/**
	 * @param ingredient
	 */
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
}
