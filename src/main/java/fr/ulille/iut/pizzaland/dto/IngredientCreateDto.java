package fr.ulille.iut.pizzaland.dto;
	
/**
 * @author gabetr
 * Classe décrivant la création de l'élément Ingredient dans le dto
 */
public class IngredientCreateDto {
	private String name;
		
	public IngredientCreateDto() {}
		
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
 		
	/**
	 * @return
	 */
	public String getName() {
		return name;
	}
}
