package fr.ulille.iut.pizzaland.dto;

/**
 * @author gabetr
 * Classe décrivant la création de l'élément Commande dans le dto
 */

public class CommandeCreateDto {
	private String name;

	public CommandeCreateDto() {}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}