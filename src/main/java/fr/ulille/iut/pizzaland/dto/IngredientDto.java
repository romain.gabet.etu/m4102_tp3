package fr.ulille.iut.pizzaland.dto;

import java.util.UUID;
/**
 * @author gabetr
 * Classe décrivant l'élément Ingredient dans le dto
 */
public class IngredientDto {
    private UUID id;
    private String name;

    public IngredientDto() {
    }

    /**
     * @param id
     */
    public void setId(UUID id) {
        this.id = id;
    }

    /**
     * @return
     */
    public UUID getId() {
        return id;
    }

    /**
     * @param name
     */
    public void setName(String name) {
      this.name = name;
    }

    /**
     * @return
     */
    public String getName() {
      return name;
    }
}