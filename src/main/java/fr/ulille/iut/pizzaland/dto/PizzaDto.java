package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;

/**
 * @author gabetr
 * Classe décrivant l'élément Pizza dans le dto avec la liste des Ingredient
 */

public class PizzaDto {

	private UUID id;
	private String name;
	private List<Ingredient> ingredients;

	/**
	 * @return
	 */
	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	/**
	 * @param ingredient
	 */
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}


	public PizzaDto() {
	}

	/**
	 * @param id
	 */
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * @return
	 */
	public UUID getId() {
		return id;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return "PizzaDtoWithIngredients [id=" + id + ", name=" + name + ", ingredients=" + ingredients + "]";
	}
}
